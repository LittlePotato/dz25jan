﻿/*
 * Точилина Полина 
 * БПИ199
 * Домашнее задание 25.01
 * Мы делали это ночью после шести пар, не судите строго. May The Force Be With Us.
 */

using System;
using System.Collections.Generic;

namespace dz
{
    class Calculator
    {
        // Делегат для операций.
        public delegate double MathOperation(double a, double b);

        // Словарь операций: key=operation, value=anonym method.
        static Dictionary<string, MathOperation> operations = new Dictionary<string, MathOperation>
        {
            ["+"] = (a, b) => a + b,
            ["-"] = (a, b) => a - b,
            ["*"] = (a, b) => a * b,
            ["/"] = (a, b) => a / b,
            ["^"] = Math.Pow,
        };

        /// <summary>
        /// Считает выражение, используя словарь операций
        /// </summary>
        /// <param name="expr"> Выражение типа "A + B", где A, B - double; + - одна из операций. </param>
        /// <returns></returns>
        public static double Calculate(string expr)
        {
            // Вы просили без if-ов, мы выживали как могли. (Parse and Zero Division)
            try
            {
                string[] arr = expr.Split();
                MathOperation op = operations[arr[1]];
                return op(double.Parse(arr[0]), double.Parse(arr[2]));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }
}
