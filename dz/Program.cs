﻿/*
 * Точилина Полина 
 * БПИ199
 * Домашнее задание 25.01
 * Мы делали это ночью после шести пар, не судите строго. May The Force Be With Us.
 */

using System;
using System.IO;

namespace dz
{
    class Program
    {
        /// <summary>
        /// Считывает все строки файла с выражениями, считает их и записывает результаты.
        /// </summary>
        /// <param name="pathFrom"> Файл с выражениями </param>
        /// <param name="pathTo"> Файл для записи результатов </param>
        static void WorkWithFile(string pathFrom, string pathTo)
        {
            try
            {
                string[] lines = File.ReadAllLines(pathFrom);
                foreach (string line in lines)
                    File.AppendAllText(pathTo, $"{Calculator.Calculate(line):F3}" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        /// <summary>
        /// Проверяет на совпадение полученных результатов с результатами из данного файла.
        /// </summary>
        /// <param name="path1"> Первый файл для сравнения. </param>
        /// <param name="path2"> Второй файл для сравнения. </param>
        /// <param name="pathTo"> Файл для записи построчного сравнения и вывода количества ошибок. </param>
        static void CheckCorrect(string path1, string path2, string pathTo)
        {
            // Тут в условии несовпадение с данными из файла. Там нет выражений, только ответы.

            string[] arr1 = File.ReadAllLines(path1);
            string[] arr2 = File.ReadAllLines(path2);
            int errors = 0;

            for(int i = 0; i < Math.Min(arr1.Length, arr2.Length); i++)
            {
                if (arr1[i] == arr2[i])
                    File.AppendAllText(pathTo, "OK" + Environment.NewLine);
                else
                {
                    errors++;
                    File.AppendAllText(pathTo, "Error" + Environment.NewLine);
                }
            }

            File.AppendAllText(pathTo, errors.ToString());
        }

        static void Main(string[] args)
        {
            WorkWithFile(@"../../../expressions.txt", @"../../../answers.txt");
            CheckCorrect(@"../../../expressions_checker.txt", @"../../../answers.txt", @"../../../results.txt");
        }
    }
}
